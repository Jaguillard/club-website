/**
 * Copyright (c) 2018 Thomas Rokicki
 */

package application.controllers.rest;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import application.models.request.user.UserPostRequest;
import application.models.request.user.UserUpdateRequest;
import application.models.response.user.User;
import application.services.accessors.UserAccessor;
import application.services.exceptions.DBICreationException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value = "api/v1/users")
@Api(value = "User", tags = { "User" })
public class UserAPI {
	private static Logger logger = Logger.getLogger(UserAPI.class);

	@ApiOperation(value = "Create User", response = User.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "CREATED", response = User.class),
			@ApiResponse(code = 400, message = "Bad Request") })
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public User post(@ApiParam(value = "User information", required = true) @RequestBody UserPostRequest request)
			throws DBICreationException {
		logger.trace("POST /users");

		User user = UserAccessor.post(request);
		return user;
	}

	@ApiOperation(value = "Update User", response = User.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Bad Request") })
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/{user-id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public User updateById(@ApiParam(value = "The user's Id") @PathVariable("user-uuid") String uuid,
			@ApiParam(value = "User information", required = true) @RequestBody UserUpdateRequest request)
			throws DBICreationException {
		logger.trace("PUT /users");
		User user = UserAccessor.updateUser(uuid, request);
		return user;
	}

	@ApiOperation(value = "Get User by Id", response = User.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Bad Request") })
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/{user-id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public User getById(@ApiParam(value = "The user's Id") @PathVariable("user-uuid") String uuid)
			throws DBICreationException {
		logger.trace("GET /users/{user-uuid}");
		User user = UserAccessor.getByUuid(uuid);
		return user;
	}

	@ApiOperation(value = "Get User by username", response = User.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Bad Request") })
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public User getByUsername(@ApiParam(value = "The username") @PathVariable("username") String username)
			throws DBICreationException {
		logger.trace("GET /users/{username}");
		User user = UserAccessor.getByUsername(username);
		return user;
	}

	@ApiOperation(value = "Delete User by Id [NOT IMPLEMENTED]", response = User.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Bad Request") })
	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(value = "/{user-id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public User deleteById(@ApiParam(value = "The user's Id") @PathVariable("user-uuid") String uuid)
			throws DBICreationException {
		logger.trace("DELETE /users/{user-uuid}");
		User user = UserAccessor.delete(uuid);
		return user;
	}

}
