/**
 * Copyright (c) 2018 Thomas Rokicki
 */

package application.models.request.user;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserUpdateRequest {

	@JsonProperty(value = "username")
	private String username;
	@JsonProperty(value = "password")
	private String password;
	@JsonProperty(value = "given_name")
	private String given_name;
	@JsonProperty(value = "surname")
	private String surname;

	UserUpdateRequest() {}

	public UserUpdateRequest(String username, String password, String given_name, String surname) {
		this.username = username;
		this.password = password;
		this.given_name = given_name;
		this.surname = surname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGiven_name() {
		return given_name;
	}

	public void setGiven_name(String given_name) {
		this.given_name = given_name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

}
