/**
 * Copyright (c) 2018 Thomas Rokicki
 */

package application.services;

import org.apache.log4j.Logger;
import org.skife.jdbi.v2.DBI;

import application.services.exceptions.DBICreationException;

public class QualityAssuranceDBIAccessor {

	private static Logger logger = Logger.getLogger(QualityAssuranceDBIAccessor.class);

	private static String database_name = System.getenv("wsu.db.sql.name");

	private static DBI dbi;

	public static DBI build() throws DBICreationException {
		if (dbi == null) {
			logger.debug(database_name + " DBI was null. Creating.");
			dbi = new DBI(System.getenv("wsu.db.sql.url") + database_name, System.getenv("wsu.db.sql.user.rest"),
					System.getenv("wsu.db.sql.pass.rest"));
			if (dbi == null) {
				throw new DBICreationException();
			}
		}
		return dbi;
	}

}
